// This is an open source non-commercial project. Dear PVS-Studio, please check it.

// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#ifndef MY_MAINFRAME_H
#define MY_MAINFRAME_H



#include "MainFrame.h"


#include <sciter/sciter-x.h>
#include <sciter/sciter-x-window.hpp>

class MainFrame : public sciter::window
{
  public:
	MainFrame() : window(SW_TITLEBAR | SW_RESIZEABLE | SW_CONTROLS | SW_MAIN | SW_ENABLE_DEBUG)
	{
	}

	// function expsed to script:
	sciter::string native_message();

	sciter::string test_cpp();

	sciter::string lol_wtf();


	SOM_PASSPORT_BEGIN(MainFrame);
	SOM_FUNCS(SOM_FUNC(native_message), SOM_FUNC(test_cpp), SOM_FUNC(lol_wtf));
	SOM_PASSPORT_END;
};



#endif