// This is an open source non-commercial project. Dear PVS-Studio, please check it.

// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#pragma once

#include <string>
#include <vector>
#include <sciter/sciter-x.h>
#include <sciter/sciter-x-window.hpp>

class SelectNativeExplorer
{
  public:
	SelectNativeExplorer()
	{
	}

	// C++ -> TiScript Save file
	static sciter::value select_file(sciter::value saveOrOpen, sciter::value fileOptions, sciter::value defaultExtension, sciter::value initialPath,
						 sciter::value dialogCaption);

	// C++ -> TiScript Open file. Returns location of opened file
	// static std::string openFileLocation(sciter::value fileOptions, sciter::value defaultExtension, sciter::value initialPath,
	// 					 sciter::value dialogCaption);

	// C++ -> TiScript Select folder
	static sciter::value select_folder(sciter::value defaultFolder, sciter::value dialogCaption);
};
