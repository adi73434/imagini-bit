// This is an open source non-commercial project. Dear PVS-Studio, please check it.

// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#pragma once

#include <string>
// #include <vector>
#include <sciter/sciter-x.h>

class Conversions
{
  public:
	// -------------------------------------------------------------------------
	// TODO: Consider passing as reference instead of return variable for some of these
	// if applicable
	// -------------------------------------------------------------------------
	static sciter::string sciter_value_to_sciter_string(sciter::value inVar);
	static std::string sciter_value_to_std_string(sciter::value inVar);
	static int sciter_value_to_int(sciter::value inVar);
	static std::string sciter_string_to_std_string(sciter::string inVar);
	
	static std::string file_name_from_file_path(std::string inVar);
	static std::string file_name_no_extension_from_file_path(std::string inVar);

	// Removes the "file://" prefix from file path
	static std::string strip_sciter_file_prefix(std::string inVar);

	static std::string encode_url(std::string inVar);

	// Decode HTML URL. Pass address of (&) var to outputStr
	static std::string decode_url(std::string inVar);
};
