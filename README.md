# imagini-bit
A C++ Image file compressor. Really, though, it's just using a HTML GUI and an existing JPEG library to reduce JPEG file sizes.

Initiated because my DSLR's JPEGs always end up over 8MB, and I'm not willing to pay for Discord Nitro. I also am too stubborn to lower the output JPEG resolution, even though I also have a RAW file for every JPEG.

## Libraries and Things
Made in Visual Studio on Windows. Windows is terrible, but I wanted the Visual Studio debugger and Sciter's inspector is broken for me on Linux. Sciter is cross-platform and I know of no conflicts so it should compile for Linux.

PVS-Studio - Analyzer thing which I mostly ignored

[Sciter](sciter.com/) - GUI etc.

stb_image - [stb](https://github.com/nothings/stb)'s image loader

[toojpeg](https://github.com/stbrumme/toojpeg) - JPEG Encoder

## Features
- Compress to around (but never over) the intended output size
- Provide feedback about save size
- Set output location using native Windows Explorer Save As dialog

## Usage
- Select inteded size
- Select precision size. This is the maximum allowed disparity between the intended size and the output size, though the JPEG quality parameter may not have the range to satisfy this.
- Setting precision size to 0 will override this and, instead, the first JPEG that satisfies "it's any amount lower than intended size" will be output
- Select intended output (or drag and drop images for the initial output directory selection)
- Drag and drop any number of JPEG, PNG, TIFF, or GIF images (Note that all are currently calculated in parralel - one thread per image)
- The intended parameters will be applied to all dragged images (You may change the parameters per batch)
- A JPEG will be generated (many times) to try to match the size. Output size cannot exceed intended size
- Output JPEGs will be made as: output directory + intput file name + `_lowquality` suffix

## Issues
- #6 Output save exception: Doesn't seem to cause any actual issues, but it's an exception nonetheless.

## Ideas:
- Add a progress bar for every batch, or some type of output box which shows which files are done and which are not, though this will increase the display footprint and I'm unsure if I want that.
- Use an INI file for user-defined defaults: default size, precision, and output starting folder