// This is an open source non-commercial project. Dear PVS-Studio, please check it.

// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#pragma once

// -----------------------------------------------------------------------------
// STD Lib
// -----------------------------------------------------------------------------

// #include <string>
#include <vector>



// -----------------------------------------------------------------------------
// Libraries
// -----------------------------------------------------------------------------

#include <sciter/sciter-x.h>
// #include <sciter/sciter-x-window.hpp>

#include <jpeg-compressor-104/jpgd.h>



class CrunchImage
{
  public:
	CrunchImage()
	{
	}

	~CrunchImage();
	
	// ---------------------------------------------------------------------
	// UI settings
	// ---------------------------------------------------------------------
	int wantOutSize;
	std::string wantOutFilepath;
	int wantPrecisionSize = 512;
	bool wantPrecision = true;

	// ---------------------------------------------------------------------
	// Drop in img
	// ---------------------------------------------------------------------
	std::string inImgPath;
	std::string inImgNameNoExt;

	// ---------------------------------------------------------------------
	// Read
	// ---------------------------------------------------------------------
	jpgd::uint8 *inImgData;
	int inImgActualComps = 0;
	int inImgRequiredComps = 3;
	int inImgWidth;
	int inImgHeight;
	int outImgBufferSize;

	// ---------------------------------------------------------------------
	// Output img
	// ---------------------------------------------------------------------
	// Used to calculate buffer size
	// Holds image output from
	void *outImgDataBuffer;
	// std::shared_ptr<void> *outImgDataBuffer;
	int outImgSize;


	void read_original_image_file();

	// Loops over image saving to memory until it's below the intended size
	void iterate_jpeg_compress_to_match_size();

	// void write_jpeg_to_file(std::string const & filename, char const * data, size_t const bytes);
	void write_jpeg_to_file();

	// Wipes the void pointer outImgDataBuffer because I don't know how/if i can wrap it in a pointer cause of the library I use
	void free_img_data_buffer();

  private:
	// Compresses JPEG to memory buffer. Pass imageBuffer as reference to hold compressed image, compSize to hold image size
	void compress_jpeg_file_to_memory(void *imageBuffer, int &compSize, int jpegQualityOut, bool runTwoPass);
};
