// This is an open source non-commercial project. Dear PVS-Studio, please check it.

// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include "CrunchImage.h"

#include "Globals.h"

#include "SelectNativeExplorer.h"



// -----------------------------------------------------------------------------
// STD Lib
// -----------------------------------------------------------------------------
#include <string>
#include <vector>
#include <stdint.h>
#include <ctype.h>
#include <fstream>
#include <iterator>
#include <algorithm>


// -----------------------------------------------------------------------------
// Libraries
// -----------------------------------------------------------------------------

// Sciter convert sciter::string to std::string
#include <sciter/aux-cvt.h>

// Read image
#define STB_IMAGE_IMPLEMENTATION
#include <stb/stb_image.c>
#include <jpeg-compressor-104/jpgd.h>

// Write JPEG iamge
// #include <toojpeg-1.5/toojpeg.h>
#include <jpeg-compressor-104/jpge.h>


CrunchImage::~CrunchImage()
{
}


void CrunchImage::read_original_image_file()
{
	// inImgWidth, inImgHeight, and inImgActualComps get modified to match the loaded image
	inImgData = stbi_load(inImgPath.c_str(), &inImgWidth, &inImgHeight, &inImgActualComps, inImgRequiredComps);

	// allocate a buffer that's hopefully big enough (this is way overkill for jpeg)
	outImgBufferSize = inImgWidth * inImgHeight * 3;
	if (outImgBufferSize < 1024)
		outImgBufferSize = 1024;
}

void CrunchImage::compress_jpeg_file_to_memory(void *imageBuffer, int &compSize, int jpegQualityOut, bool runTwoPass)
{
	jpge::uint subsampling = jpge::H2V2;

	jpge::params params;
	params.m_quality = jpegQualityOut;
	params.m_subsampling = static_cast<jpge::subsampling_t>(subsampling);
	params.m_two_pass_flag = runTwoPass;

	bool a = jpge::compress_image_to_jpeg_file_in_memory(imageBuffer, compSize, inImgWidth, inImgHeight, inImgRequiredComps, inImgData, params);

	// Something failed. I don't care: it shouldn't.
	if (!a)
		return;
}

// void CrunchImage::write_jpeg_to_file(std::string const &filename, char const *data, size_t const bytes)
void CrunchImage::write_jpeg_to_file()
{
	// https://stackoverflow.com/a/22530874
	// I don't really have any clue how this works...
	// But the dynamically allocated memory that outImgDataBuffer points to is converted into a char const* ???
	// and that works. I don't know
	const char *convertedBuffer = reinterpret_cast<char const *>(outImgDataBuffer);

	std::ofstream writeJpeg(wantOutFilepath.c_str(), std::fstream::out | std::fstream::binary);

	if (writeJpeg)
	{
		writeJpeg.write(convertedBuffer, outImgSize);
	}
}

void CrunchImage::iterate_jpeg_compress_to_match_size()
{
	outImgDataBuffer = malloc(outImgBufferSize);
	// outImgDataBuffer = new char[outImgBufferSize];
	outImgSize = outImgBufferSize;

	std::ofstream flsiz("E:\\Adrian\\Desktop\\tt.txt");

	int qualityIteration = 100;
	int lastQualityWhenTooHigh = 100;
	int lastQualityWhenTooLow = 0;
	int lastQualityRaiseSize = 4;
	bool outImgAsDesired = false;

	while (!outImgAsDesired)
	{
		flsiz << "Quality: " << qualityIteration << std::endl
			  << "imageBuffer: " << outImgDataBuffer << std::endl
			  << "imgSize: " << outImgSize
			  << std::endl
			  //   << "wasARaise: " << lastIterationWasARaise << std::endl
			  << "lastQualityRaiseSize: " << lastQualityRaiseSize << std::endl
			  << std::endl;
		// ---------------------------------------------------------------------
		// FIXME: Output image size just becomes zero????
		// ---------------------------------------------------------------------
		compress_jpeg_file_to_memory(outImgDataBuffer, outImgSize, qualityIteration, true);
		// Bytes into KiB
		int currentOutSize = outImgSize / 1024;

		// If quality is already maxed
		if (currentOutSize < wantOutSize && qualityIteration == 100)
		{
			outImgAsDesired = true;
		}
		else if (!wantPrecision && currentOutSize > wantOutSize)
		{
			lastQualityWhenTooHigh = qualityIteration;
			qualityIteration = qualityIteration - 5;
		}
		else if (wantPrecision && currentOutSize > wantOutSize)
		{
			lastQualityWhenTooHigh = qualityIteration;
			// Make bigger steps down if the too-low quality isn't close to current (aka not established yet)
			if (qualityIteration - lastQualityWhenTooLow > 5)
				qualityIteration = qualityIteration - 3;
			else
				qualityIteration--;
		}
		// If user wanted precision and output size is too small
		else if (wantPrecision && currentOutSize < wantOutSize && currentOutSize < wantOutSize - wantPrecisionSize)
		{
			lastQualityWhenTooLow = qualityIteration;

			// If next raise is one that makes file too large, finish
			if (qualityIteration + 1 == lastQualityWhenTooHigh)
			{
				outImgAsDesired = true;
			}
			else
			{
				qualityIteration++;
				// Also with the way jpge library works, I think outImgSize needs to be
				// larger than the possible output, so since in this step the quality is going up
				// the size will go up, so reset the outImgSize
				// NOTE: Copying this over seems very inefficient, but fuck if I know how else to do it
				outImgSize = outImgBufferSize;
			}
		}
		// Assume here that currentOutSize is lower than wantOutSize, but not more than desired precision
		// or the user doesn't care about precision
		else
		{
			outImgAsDesired = true;
		}
	}

	flsiz.close();
}



void CrunchImage::free_img_data_buffer()
{
	// Where thee maketh with new, thee shalt deletus by thyself
	// Free the malloc created:
	free(inImgData); // stbi_load
	free(outImgDataBuffer); // iterate_jpeg_compress_to_match_size
}