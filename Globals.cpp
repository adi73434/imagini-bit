// This is an open source non-commercial project. Dear PVS-Studio, please check it.

// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include "Globals.h"


int Globals::myglobalint = 78;
sciter::om::hasset<MainFrame> Globals::mainPwin;
HWND Globals::mainHwnd;