// This is an open source non-commercial project. Dear PVS-Studio, please check it.

// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include "HandleEvents.h"

// This globals.h files gives the global variable that is needed to call TiScript functions from C++
#include "Globals.h"

#include "Conversions.h"
#include "CrunchImage.h"
#include "SelectNativeExplorer.h"



// -----------------------------------------------------------------------------
// STD Lib

#include <string>
#include <vector>
#include <thread>
#include <mutex>


// -----------------------------------------------------------------------------
// Libraries
// -----------------------------------------------------------------------------

// Sciter convert sciter::string to std::string
#include <sciter/aux-cvt.h>

#include <sciter/sciter-x.h>
#include <sciter/sciter-x-host-callback.h>
#include <sciter/sciter-x-threads.h>


// -----------------------------------------------------------------------------
// This is really ugly, but in order to have access to these in
// threaded_image_process()
// they needed to be static
// -----------------------------------------------------------------------------
int HandleEvents::uiCurrentOutSize = 7168;
int HandleEvents::uiSizePrecision = 512;
bool HandleEvents::uiCareAboutPrecision = true;
std::string HandleEvents::uiCurrentOutPath = "";

std::mutex HandleEvents::jpegWriteLock;


HandleEvents::~HandleEvents()
{
}


sciter::value HandleEvents::tocpp_set_image_desired_size(sciter::value varFromUi)
{
	uiCurrentOutSize = Conversions::sciter_value_to_int(varFromUi);

	// Return void
	return sciter::value();
}

sciter::value HandleEvents::tocpp_set_size_precision(sciter::value varFromUi)
{
	int uiSizeInput = Conversions::sciter_value_to_int(varFromUi);
	// if larger than zero (and also non negative), use size precision
	if (uiSizeInput > 0)
	{
		uiCareAboutPrecision = true;
		uiSizePrecision = uiSizeInput;
	}
	else
	{
		uiCareAboutPrecision = false;
	}
	return sciter::value();
}

sciter::value HandleEvents::cpp_set_output_image_location()
{
	sciter::value selectedFolder = SelectNativeExplorer::select_folder("C:\\", "Select where to save compressed JPEG");
	uiCurrentOutPath = Conversions::sciter_value_to_std_string(selectedFolder);

	// Return void
	return sciter::value();
}

bool HandleEvents::recheck_output_image_location()
{
	// Check current start
	if (uiCurrentOutPath == "" || uiCurrentOutPath == "undefined")
	{
		// Reprompt and check if now finally set
		cpp_set_output_image_location();
		if (uiCurrentOutPath == "" || uiCurrentOutPath == "undefined")
		{
			// Give up
			return false;
		}
	}
	return true;
};

void threaded_image_process(CrunchImage newJpeg, std::string strImgPath)
{
	newJpeg.inImgNameNoExt = Conversions::file_name_no_extension_from_file_path(strImgPath);
	newJpeg.wantPrecision = HandleEvents::uiCareAboutPrecision;
	newJpeg.wantPrecisionSize = HandleEvents::uiSizePrecision;
	newJpeg.wantOutSize = HandleEvents::uiCurrentOutSize;
	newJpeg.wantOutFilepath = HandleEvents::uiCurrentOutPath + "/" + newJpeg.inImgNameNoExt + "_lowquality.jpeg";
	newJpeg.inImgPath = strImgPath;
	newJpeg.read_original_image_file();
	newJpeg.iterate_jpeg_compress_to_match_size();

	// Only write one file at once. This probably isn't an issue on SSDs but oh well
	HandleEvents::jpegWriteLock.lock();
	newJpeg.write_jpeg_to_file();
	HandleEvents::jpegWriteLock.unlock();
	newJpeg.free_img_data_buffer();
}

struct HandleEvents::thread_params
{
	sciter::value uiCallback; // "progress" callback, function passed from script
	sciter::value uiImgPaths;
	int uiNumOfItems;
};

// This couldn't be a class member because it wouldn't want to work with
// sciter::thread(unblockui_image_dropped, params);
// consequently, threaded_image_process was also gutted to be a non class member
// and I just pass the object there instead and made some statics
void unblockui_image_dropped(HandleEvents::thread_params params)
{
	// Convert all uiImgPaths to std::string
	std::vector<std::string> strImgPaths;

	for (int i = 0; i < params.uiNumOfItems; i++)
	{
		strImgPaths.push_back(Conversions::sciter_value_to_std_string(params.uiImgPaths[i]));
	}

	// TODO: If too many images added, check and split it into batches of 100 (?)
	const size_t numOfThreads = strImgPaths.size();

	std::vector<std::thread> ThreadJpeg;
	// Store all the objects for each thread here
	// std::vector<CrunchImage> ThreadJpegObjects;

	ThreadJpeg.reserve(numOfThreads);

	// For every thread, create an object and spawn thread and pass it the iterated image path
	for (int i = 0; i < numOfThreads; i++)
	{
		CrunchImage newJpeg;
		ThreadJpeg.emplace_back(std::thread(threaded_image_process, std::cref(newJpeg), strImgPaths[i]));

		// Where thee maketh with new, thee shalt deletus by thyself
	}

	// Join back together all of the threads
	for (int i = 0; i < ThreadJpeg.size(); i++)
	{
		ThreadJpeg[i].join();
	}


	params.uiCallback.call();
};

sciter::value HandleEvents::tocpp_image_dropped(sciter::value uiImgPaths, sciter::value uiNumOfItems, sciter::value uiCallback)
{
	int intNumOfitems = Conversions::sciter_value_to_int(uiNumOfItems);
	if (!recheck_output_image_location())
	{
		return WSTR("noOutputSet");
	}

	thread_params params;
	params.uiCallback = uiCallback;
	params.uiImgPaths = uiImgPaths;
	params.uiNumOfItems = intNumOfitems;

	// Call a sciter thread so the GUI can continue. unblockui_image_dropped will spawn
	// threads for each image itself
	sciter::thread(unblockui_image_dropped, params);
	// Return void
	return sciter::value();
}