// This is an open source non-commercial project. Dear PVS-Studio, please check it.

// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#ifndef MY_GLOBALS_H
#define MY_GLOBALS_H



#include <sciter/sciter-x.h>
#include <sciter/sciter-x-window.hpp>

#include "MainFrame.h"

class Globals
{
  public:
	static int myglobalint;
	static sciter::om::hasset<MainFrame> mainPwin;
	static HWND mainHwnd;
};



#endif