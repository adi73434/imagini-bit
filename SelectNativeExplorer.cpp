// This is an open source non-commercial project. Dear PVS-Studio, please check it.

// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include "SelectNativeExplorer.h"

#include "Globals.h"

sciter::value SelectNativeExplorer::select_file(sciter::value saveOrOpen, sciter::value fileOptions,
										  sciter::value defaultExtension, sciter::value initialPath,
										  sciter::value dialogCaption)
{
	sciter::value params[5] = {saveOrOpen, fileOptions, defaultExtension, initialPath, dialogCaption};
	// I would like to use this to get the number of elements in the array, but because in C++ apparently you
	// only know the size of arrays at compiler-time, I think I can't really use a "one function for all" TiScript-function-caller
	// int numOfParams = params->length();
	// int numOfParams = sizeof(params);

	// -------------------------------------------------------------------------
	// Call TiScript function, method 1
	// -------------------------------------------------------------------------
	// if (SciterCall(Globals::mainHwnd, "nativeExplorerFile", numOfParams, params, &returnValue))
	// {
	// 	int ri = returnValue.get(0);
	// }

	// -------------------------------------------------------------------------
	// Call TiScript function, method 2
	// -------------------------------------------------------------------------

	return Globals::mainPwin->call_function("nativeExplorerFile", sciter::value(params, 5));
}

// std::string SelectNativeExplorer::openFile(sciter::value fileOptions,
// 										  sciter::value defaultExtension, sciter::value initialPath,
// 										  sciter::value dialogCaption)
// {
// 	sciter::value params[4] = {fileOptions, defaultExtension, initialPath, dialogCaption};
// 	// I would like to use this to get the number of elements in the array, but because in C++ apparently you
// 	// only know the size of arrays at compiler-time, I think I can't really use a "one function for all" TiScript-function-caller
// 	// int numOfParams = params->length();
// 	// int numOfParams = sizeof(params);

// 	sciter::value returnValue;

// 	// -------------------------------------------------------------------------
// 	// Call TiScript function, method 1
// 	// -------------------------------------------------------------------------
// 	// if (SciterCall(Globals::mainHwnd, "nativeExplorerFile", numOfParams, params, &returnValue))
// 	// {
// 	// 	int ri = returnValue.get(0);
// 	// }

// 	// -------------------------------------------------------------------------
// 	// Call TiScript function, method 2
// 	// -------------------------------------------------------------------------
// 	returnValue = Globals::mainPwin->call_function("Native.saveFile", sciter::value(params, 4));
// }

sciter::value SelectNativeExplorer::select_folder(sciter::value defaultFolder, sciter::value dialogCaption)
{
	sciter::value params[2] = {defaultFolder, dialogCaption};

	return Globals::mainPwin->call_function("nativeExplorerFolder", sciter::value(params, 2));
}

// -----------------------------------------------------------------------------
// Call TiScript function, pass an array
// https://sciter.com/forums/topic/send-c-vector-object-to-tiscript/
//
// sciter::value retval;
// sciter::value parentData; // will be set as array by parentData.append() below
// for (int i = 1; i <= 100; i++)
// {
// 	sciter::value childData;
// 	childData["id"] = sciter::value(i);
// 	childData["full_name"] = sciter::value(L"John Doe");
// 	childData["mobile_number"] = sciter::value(L"0123456789");
// 	parentData.append(childData); // set parentData as array and/or append element to it
// }
// SciterCall(Globals::mainHwnd, "nativeExplorerFile", 1, &parentData, &retval);
// -----------------------------------------------------------------------------