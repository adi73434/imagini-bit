// This is an open source non-commercial project. Dear PVS-Studio, please check it.

// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include "MainFrame.h"

// function expsed to script:
sciter::string MainFrame::native_message()
{
	return WSTR("FromC++ - C++ World");
}

sciter::string MainFrame::test_cpp()
{
	return WSTR("FromC++ - G++");
}

sciter::string MainFrame::lol_wtf()
{

	sciter::window::call_function("alertUserTest");

	return WSTR("1");
}
