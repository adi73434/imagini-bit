// This is an open source non-commercial project. Dear PVS-Studio, please check it.

// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include "Conversions.h"

#include <string>
#include <cctype>
#include <iomanip>
#include <sstream>
// #include <vector>
#include <sciter/sciter-x.h>

// Sciter convert sciter::string to std::string
#include <sciter/aux-cvt.h>



sciter::string Conversions::sciter_value_to_sciter_string(sciter::value inVar)
{
	return inVar.get<sciter::string>();
}

std::string Conversions::sciter_value_to_std_string(sciter::value inVar)
{
	// -------------------------------------------------------------------------
	// Convert from sciter::value to sciter::string
	// using template
	// Then convert from sciter::string to std::string
	// https://sciter.com/forums/topic/how-to-convert-the-sciter-string-to-std-string/
	// -------------------------------------------------------------------------

	// sciter::string stepOne = inVar.get<sciter::string>();
	// std::string stepTwo = aux::w2a(stepOne);

	return aux::w2a(inVar.get<sciter::string>());
}

int Conversions::sciter_value_to_int(sciter::value inVar)
{
	return inVar.get<int>();
}

std::string Conversions::sciter_string_to_std_string(sciter::string inVar)
{
	return aux::w2a(inVar);
}

std::string Conversions::file_name_from_file_path(std::string inVar)
{
	return inVar.substr(inVar.find_last_of("/\\") + 1);
}

std::string Conversions::file_name_no_extension_from_file_path(std::string inVar)
{
	// std::string stepOne = inVar.substr(inVar.find_last_of("/\\") + 1);
	// size_t lastIndex = stepOne.find_last_of(".");
	// std::string stepTwo = stepOne.substr(0, lastIndex);

	std::string fileNameWithExtension = inVar.substr(inVar.find_last_of("/\\") + 1);
	return fileNameWithExtension.substr(0, fileNameWithExtension.find_last_of("."));
}

std::string Conversions::strip_sciter_file_prefix(std::string inVar)
{
	return inVar.erase(0, 7);
}

std::string Conversions::encode_url(std::string inVar)
{
	// // https://stackoverflow.com/a/29962178
	// std::string outStr = "";
	// char c;
	// int ic;
	// const char *inChars = inVar.c_str();
	// char bufHex[10];
	// int len = strlen(inChars);

	// for (int i = 0; i < len; i++)
	// {
	// 	c = inChars[i];
	// 	ic = c;
	// 	// uncomment this if you want to encode spaces with +
	// 	/*if (c==' ') outStr += '+';
	// 	else */
	// 	if (isalnum(c) || c == '-' || c == '_' || c == '.' || c == '~')
	// 		outStr += c;
	// 	else
	// 	{
	// 		sprintf(bufHex, "%X", c);
	// 		if (ic < 16)
	// 			outStr += "%0";
	// 		else
	// 			outStr += "%";
	// 		outStr += bufHex;
	// 	}
	// }
	// return outStr;

	// https://stackoverflow.com/a/17708801
    std::ostringstream escaped;
    escaped.fill('0');
    escaped << std::hex;

    for (std::string::const_iterator i = inVar.begin(), n = inVar.end(); i != n; ++i) {
        std::string::value_type c = (*i);

        // Keep alphanumeric and other accepted characters intact
        if (isalnum(c) || c == '-' || c == '_' || c == '.' || c == '~') {
            escaped << c;
            continue;
        }

        // Any other characters are percent-encoded
        escaped << std::uppercase;
        escaped << '%' << std::setw(2) << int((unsigned char) c);
        escaped << std::nouppercase;
    }

    return escaped.str();
}

std::string Conversions::decode_url(std::string inVar)
{
	// -------------------------------------------------------------------------
	// TODO: Make this decode instead of encode
	// TODO: Make this decode instead of encode
	// TODO: Make this decode instead of encode
	// TODO: Make this decode instead of encode
	// TODO: Make this decode instead of encode
	// TODO: Make this decode instead of encode
	// TODO: Make this decode instead of encode
	// TODO: Make this decode instead of encode
	// TODO: Make this decode instead of encode
	// TODO: Make this decode instead of encode
	// TODO: Make this decode instead of encode
	// TODO: Make this decode instead of encode
	// TODO: Make this decode instead of encode
	// TODO: Make this decode instead of encode
	// TODO: Make this decode instead of encode
	// TODO: Make this decode instead of encode
	// TODO: Make this decode instead of encode
	// TODO: Make this decode instead of encode
	// TODO: Make this decode instead of encode
	// TODO: Make this decode instead of encode
	// -------------------------------------------------------------------------
	std::ostringstream escaped;
    escaped.fill('0');
    escaped << std::hex;

    for (std::string::const_iterator i = inVar.begin(), n = inVar.end(); i != n; ++i) {
        std::string::value_type c = (*i);

        // Keep alphanumeric and other accepted characters intact
        if (isalnum(c) || c == '-' || c == '_' || c == '.' || c == '~') {
            escaped << c;
            continue;
        }

        // Any other characters are percent-encoded
        escaped << std::uppercase;
        escaped << '%' << std::setw(2) << int((unsigned char) c);
        escaped << std::nouppercase;
    }

    return escaped.str();
}
