// This is an open source non-commercial project. Dear PVS-Studio, please check it.

// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#pragma once

#include <sciter/sciter-x.h>

#include "CrunchImage.h"

#include <thread>
#include <mutex>


class HandleEvents : public sciter::event_handler
{
	HWND _hwnd;

  public:
	HandleEvents()
	{
	}

	~HandleEvents();

	// Currently selected in UI settings
	static int uiCurrentOutSize;
	// Precise size is used to try to make the JPEG closer to the desired output
	// if one of the initial compressions was much smaller than desired
	static int uiSizePrecision;
	static bool uiCareAboutPrecision;
	static std::string uiCurrentOutPath;

	sciter::value tocpp_set_image_desired_size(sciter::value varFromUi);
	sciter::value tocpp_set_size_precision(sciter::value varFromUi);
	sciter::value cpp_set_output_image_location();

	static std::mutex jpegWriteLock;

	// Thread per each image. Takes already created object from unblockui_image_dropped
	// void threaded_image_process(CrunchImage &newJpeg, std::string strImgPath);

	struct thread_params;

	// Spawn std::threads in a for loop for every image
	// void unblockui_image_dropped(thread_params params);
	sciter::value tocpp_image_dropped(sciter::value uiImgPaths, sciter::value uiNumOfItems, sciter::value uiCallback);


	// Sample stuff
	// void toui_sample_call();
	// sciter::value fromcpp_return_test_string();


	// Thread testing: REMOVE
	// void thread_body(sciter::value uiCallback);
	// void test_thread(sciter::value uiCallback);


	// -----------------------------------------------------------------------------
	// SECTION: Expose to Sciter
	// -----------------------------------------------------------------------------
	SOM_PASSPORT_BEGIN(HandleEvents)
	SOM_FUNCS(SOM_FUNC(tocpp_set_image_desired_size), SOM_FUNC(tocpp_set_size_precision), SOM_FUNC(cpp_set_output_image_location),
			  SOM_FUNC(tocpp_image_dropped));
	SOM_PASSPORT_END;
	// BEGIN_FUNCTION_MAP
	// FUNCTION_1("tocpp_set_image_desired_size", tocpp_set_image_desired_size);
	// FUNCTION_1("tocpp_set_size_precision", tocpp_set_size_precision);
	// FUNCTION_0("cpp_set_output_image_location", cpp_set_output_image_location);
	// FUNCTION_3("tocpp_image_dropped", tocpp_image_dropped);
	// END_FUNCTION_MAP
	// !SECTION


  private:
	// Check that the user set an output image location and re-prompt if none set
	bool recheck_output_image_location();
};
