
// -----------------------------------------------------------------------------
// This is essentially the main "loop"/portion of the image saver
// -----------------------------------------------------------------------------
// sciter::value HandleEvents::tocpp_compute_save_button_click(sciter::value taskId, sciter::value progressCb, sciter::value uiCallback)
// {
// 	// std::thread lolThread(&HandleEvents::thread_body);
// 	// lolThread.detach();
// 	std::thread lolThread(&HandleEvents::thread_body, this, uiCallback);
// 	lolThread.detach();
// 	return sciter::value(); // void method
// }


// void HandleEvents::toui_sample_call()
// {
// 	Globals::mainPwin->call_function("tesst");
// }



// sciter::value HandleEvents::fromcpp_return_test_string()
// {
// 	// -------------------------------------------------------------------------
// 	// This returns sample array; vector is dynamic size
// 	// -------------------------------------------------------------------------
// 	// std::vector<sciter::value> vectorData;
// 	// for (int i = 0; i < 9; i++)
// 	// {
// 	// vectorData.push_back(i);
// 	// }
// 	// return vectorData;

// 	// -------------------------------------------------------------------------
// 	// This returns list (?) of things; you have to specify the size
// 	// -------------------------------------------------------------------------
// 	// sciter::value vectorData[3] = { "1", "2", "3"};
// 	// return sciter::value(vectorData, 3);

// 	// -------------------------------------------------------------------------
// 	// This returns class state of image locations, or a default "no value"
// 	// -------------------------------------------------------------------------
// 	return "feedback_goes_here";
// }